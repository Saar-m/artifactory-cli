# Create user 
``` $artctl create-user [username] [password] [email] ```
Creates a new user in Artifactory, allows you to switch contexts to the newly created user.

# API reference
[Description:](https://www.jfrog.com/confluence/display/JFROG/Artifactory+REST+API#ArtifactoryRESTAPI-CreateorReplaceUser)
Since: 2.4.0
Notes: Requires Artifactory Pro
Missing values will be set to the default values as defined by the consumed type.
Security: Requires an admin user
Usage: PUT /api/security/users/{userName}
Consumes: application/json (application/vnd.org.jfrog.artifactory.security.User+json)
Sample Usage:
```
PUT /api/security/users/davids
{
user.json
}
```

[JSON](https://www.jfrog.com/confluence/display/JFROG/Security+Configuration+JSON#SecurityConfigurationJSON-application/vnd.org.jfrog.artifactory.security.User+json)

Legend

+ Mandatory element in create/replace queries, optional in "update" queries

- Optional element in create/replace queries

! Read-only element

(default) The default value when unspecified in create/replace queries

Object: application/vnd.org.jfrog.artifactory.security.User+json
```json
{
  - "name": "davids",
  + "email" : "davids@jfrog.com",
  + "password": "***" (write-only, never returned),
  - "admin": false (default),
  - "profileUpdatable": true (default),
  - "disableUIAccess" : true,
  - "internalPasswordDisabled": false (default),
  ! "lastLoggedIn": ISO8601 (yyyy-MM-dd'T'HH:mm:ss.SSSZ),
  ! "realm": "Internal",
  - "groups" : [ "deployers", "users" ],
  - "watchManager": false(default),
  - "policyManager": false(default)
}
```