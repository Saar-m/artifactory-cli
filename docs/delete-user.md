# Delete User
``` $artctl delete-user [username] ```
Removes an Artifactory user.

# API reference
[Description:](https://www.jfrog.com/confluence/display/JFROG/Artifactory+REST+API#ArtifactoryRESTAPI-CreateorReplaceUser) 
Notes: Requires Artifactory Pro
Security: Requires an admin user
Usage: DELETE /api/security/users/{userName}
Produces: application/text
Sample Usage:

DELETE /api/security/users/davids
 
User 'davids' has been removed successfully.