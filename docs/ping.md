# System Health Ping
``` $ artctl ping ```
Displays 'OK' text if Artifactory is working properly, if not will return an HTTP error code with a reason.
# API reference
[Description:](https://www.jfrog.com/confluence/display/JFROG/Artifactory+REST+API#ArtifactoryRESTAPI-SystemHealthPing)
Get a simple status response about the state of Artifactory
Security: Requires a valid user (can be anonymous).  If artifactory.ping.allowUnauthenticated=true is set in artifactory.system.properties, then no authentication is required even if anonymous access is disabled.
Usage: GET /api/system/ping
Produces: text/plain
Sample Output:
GET /api/system/ping
```
OK
```