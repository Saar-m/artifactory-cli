# Version and Add-ons information
``` $ artctl version ```
Displays information about the current Artifactory version.
# API reference 
[Description:](https://www.jfrog.com/confluence/display/JFROG/Artifactory+REST+API#ArtifactoryRESTAPI-VersionandAdd-onsinformation)
Security: Requires a valid user (can be anonymous)
Usage: GET /api/system/version
Produces: application/json (application/vnd.org.jfrog.artifactory.system.Version+json)
Sample Output:
```json
{
  "version" : "2.2.2",
  "revision" : "10427",
  "addons" : [ "build", "ldap", "properties", "rest", "search", "sso", "watch", "webstart" ]
}
```