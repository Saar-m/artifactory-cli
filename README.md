#  artctl - Artifactory CLI

artctl allows you to easily manage some common Jfrog Artifactory resources directly from the terminal. Based on the API it communicates with your Jfrog Artifactory SaaS instance.
Currently there are five supported commands and it was built with extensibility in mind. 
# :key: Installation

```bash
$ git clone git@gitlab.com:Saar-m/artifactory-cli.git
$ cd artifactory-cli
$ ./artctl init
```
# :computer: Usage
Each command has its own readme and presets. They share [common utilities](common) .

:vertical_traffic_light: [System ping](docs/ping.md) ``` $ artctl ping ``` 

:open_file_folder:  [System storage information.](docs/storage-info.md) ``` $ artctl storage-info ```

:information_source: [System version](docs/version.md) ``` $ artctl version ```

:busts_in_silhouette: [Create user](docs/create-user.md) ``` $ artctl create-user [username] [password] [email] ```

:bust_in_silhouette: [Delete user](docs/delete-user.md) ``` $ artctl delete-user [username] ```

:key: [Initialize artctl](init.md) ``` $ artctl init ```


# :thought_balloon: Feedback

If you have any suggestions or just want to let me know what you think of artcli, please send a [message!](mailto:hire.saar@gmail.com) .